# Videogame tournament organiser web application #


### Creating the NodeJS Project ###
* npm init

### Installing the dependencies ###
* Express: npm install express --save;
* Body-parser: npm install body-parser --save;
* Sequelize: npm install sequelize --save;
* MySQL connector: npm install mysql2 --save;

### Importing the database - backup-database.sql ###

### Endpoints - tested with Postman on localhost:4111/ ###
* GET REQUEST - GET/ALL:-ENTITIES -> get/all-users, get/all-games, get/all-tournaments, get/all-tournament_participants

* GET REQUEST - GET/:ENTITY/:ID -> get/user/1, get/game/1, get/tournament/1

* POST REQUEST - ADD/:ENTITY -> add/user, add/game, add/tournament, add/tournament_participant 


* PUT REQUEST - UPDATE/:ENTITY/:ID -> update/user/1, update/game/1, update/tournament/1, update/tournament_participant/1

* DELETE REQUEST - DELETE/:ENTITY/:ID -> delete/user/1, delete/game/1


### Examples of JSON s for testing the application
* USER -> {
  "id": 7,
  "username": "username7",
  "user_password": "POST",
  "email" : "post@added.com",
  "user_type" : "player" 
}

* GAME -> {
	"id": 7,
	"game_name": "game7",
	"game_description": "This game was added with POST!"	}
* TOURNAMENT ->{
   "id":7,
   "tournament_name": "ADDED tournament",
   "game_id": 1,
   "location": "Bucharest",
   "tournament_date": "2018-04-04",
   "organiser_id":3 }
* TOURNAMENT_PARTICIPANT ->{
	"tournament_id":7,
	"user_id":7
} 


