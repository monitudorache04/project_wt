//Dependencies
const express = require("express");
const bodyParser = require("body-parser");
const Sequelize = require("sequelize");


const app = express();    
app.use(bodyParser.json());

//Establishing the connection with the database
const sequelize = new Sequelize("wt_database", "root", "1234",{
	host:"localhost",
	dialect:"mysql",
	//operatorsAliases: false
    //logging: false,
    define: {
        timestamps: false
    }
})


//Testing the connection with the database
sequelize.authenticate().then(() => {
    console.log('Connection has been established successfully.');})
    .catch(err => {
    console.error('Unable to connect to the database:', err);
  });





//Tables in our database -> users, games, tournaments, tournament_participants
 const users = sequelize.define("users",{
	//columns
	id:{
	   type: Sequelize.INTEGER(30), 
	   allowNull: false,            
	   primaryKey:true,              
	   autoIncrement: true 
	},
	username:{
		type: Sequelize.STRING,   
		allowNull: false,
		unique:true
	},
	user_password:{
		type:Sequelize.STRING(10),
		allowNull:false
	},
	email:{
		type:Sequelize.STRING(10),
		allowNull: true,
		unique:false 

	},
	user_type:{
		type:Sequelize.ENUM('admin', 'player'),
		allowNull: true
	}
})


const games = sequelize.define("games",{
	//columns
	id:{
	   type: Sequelize.INTEGER(30), 
	   allowNull: false,            
	   primaryKey:true,             
	   autoIncrement: true 
	},
	game_name:{
		type: Sequelize.STRING,   
		allowNull: false,
		unique:true
	},
	game_description:{
		type:Sequelize.STRING,
		allowNull:true  
	}
	
})


const tournaments = sequelize.define("tournaments",{
	id:{
		type:Sequelize.INTEGER(30),
		allowNull: false,
		primaryKey: true,
		autoIncrement: true
	},
	tournament_name:{
		type:Sequelize.STRING,
		allowNull:false
	},
	game_id:{                         
		type:Sequelize.INTEGER(30),
		allowNull: true, 
		references: "games", 
        referencesKey: "id" 
	},
	location:{
		type:Sequelize.STRING,
    	allowNull:true
	},
	tournament_date:{
		type: Sequelize.DATE,
		allowNull:true
	},
	organiser_id:{
		type:Sequelize.INTEGER(30),
		allowNull:true,
		references: "users",
		referencesKey: "id"
	}
})




const tournament_participants = sequelize.define("tournament_participants", {
	tournament_id:{
		type:Sequelize.INTEGER(30),
		allowNull: false,
		primaryKey:true,
		references: "tournaments",
		referencesKey: "id"
	},
	user_id:{
		type:Sequelize.INTEGER(30),
		allowNull: false,
		primaryKey: true,
		references: "users",
		referencesKey: "id"
	}
})


//Helper function for entities (users, games, tournaments, tournament_participants -> in plural
//This function takes as parameter the entities that we write in our request (users, games)
function getEntities(entitiesName) {
	switch (entitiesName) {
		case "users":
			return users;
		case "games":
			return games;
		case "tournaments":
		    return tournaments;
		case "tournament_participants":
		    return tournament_participants;
	}
}


 //GET REQUEST -> Displaying all entities of a type that are found in the database
 //->get/all-users, get/all-games, get/all-tournaments, get/all-tournament_participants
 app.get("/get/all-:entities", async (req, res) => {
	try {
        //Getting the entities from the database
        let entities = await getEntities(req.params.entities).findAll();
		res.status(200).json(entities); 
	}
	catch (e) {
		console.warn(e)
		res.status(500).json({ message: "Error!" })
	}
})



//Helper function for entity (user, game, tournament, tournament_participant) -> in singular 
function getEntity(entityName){
	switch(entityName){
		case "user": 
		     return users;
		case "game":
		     return games;
		case "tournament":
		     return tournaments;
		case "tournament_participant":
		     return tournament_participants

	}

}

 //GET REQUEST -> Displaying the entity with a certain id
 //->get/user/1, get/game/1, get/tournament/1 
app.get("/get/:entity/:id", async(req,res)=>{
	try{
		//Finding the entity 
		let foundEntity = await getEntity(req.params.entity).findById(req.params.id);

		if(foundEntity)
		{
			res.status(201).json(foundEntity);
		}
		else
		{
			res.status(404).json({message: req.params.entity + " not found!"});
		}

	}
	catch (e) {
		console.warn(e)
		res.status(500).json({ message: "Error!" })
	}

})


//POST REQUEST -> Adding an entity to the database
//->add/user, add/game, add/tournament
app.post("/add/:entity", async(req,res)=>{
	try {
		 await getEntity(req.params.entity).create(req.body); 
		 res.status(201).send("The " + req.params.entity + " has been successfully added to the database!");
	
	}
	catch (e) {
		console.warn(e)
		res.status(500).json({ message: "Error!" })
	}

})


//PUT REQUEST -> Updating a certain entity from the database
//->update/user/1, update/game/1
app.put("/update/:entity/:id", async(req,res)=>{
	try {
		//Finding the entity with the certain id 
		let foundEntity = await getEntity(req.params.entity).findById(req.params.id)

		//if the entity has been found we will update the database entry 
		if (foundEntity) {

			await foundEntity.update(req.body) //updating the user in the database
			res.status(202).send("You have successfully updated the database!");
		}
		else {
			res.status(404).json({ message: req.params.entity + " not found!" });
		}
	}
	catch (e) {
		console.warn(e)
		res.status(500).json({ message: "ERROR!"})
	}

})


//DELETE REQUEST -> Deleting an entity with a certain id from the database
//->delete/game/1, delete/game/2
app.delete("/delete/:entity/:id", async(req,res)=>{
	try{
	let foundEntity = await getEntity(req.params.entity).findById(req.params.id);

	if(foundEntity){
		await foundEntity.destroy();
		res.status(202).send("The " + req.params.entity + " has been succesfully deleted from the database!");
	}
	else
	{
		res.status(404).json({message: req.params.entity + " not found!"})
	}
    }
    catch(e){
    	console.warn(e)
    	res.status(500).json({message: "ERROR!"})
    }

})




app.listen(4111, ()=>{
	console.log("The server started running on port 4111..");
})




